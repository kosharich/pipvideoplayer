#include "Barrier.h"

#include <QMutexLocker>

#include <iostream>

CycledBarrier::CycledBarrier(uint count)
    : _count(count)
    , _counter(count)
    , _isReset(false)
{}

CycledBarrier::~CycledBarrier()
{
}

void CycledBarrier::wait()
{
    QMutexLocker lock(&_mutex);

    if (_isReset)
        return;
    
    if (_counter == 0)
    {
        _counter = _count;
        _waitCondition.wakeAll();
    }
    else
    {
        --_counter;
        _waitCondition.wait(&_mutex);
    }
}

void CycledBarrier::reset()
{
    QMutexLocker lock(&_mutex);
    
    _isReset = true;
    _waitCondition.wakeAll();
}

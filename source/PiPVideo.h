#pragma once

#include "IFrameReadyNotifier.h"

#include <QString>
#include <QImage>
#include <QScopedPointer>

class Video;
class Frame;
class CycledBarrier;

class PiPVideo : private IFrameReadyNotifier
{
public:
    PiPVideo(const QString mainVideoPath, const QString embededVideoPath);
    ~PiPVideo();

    Frame* getFrame();

    double fps() const;

private:
    virtual void notify();
    void waitFrames();

private:
    QString _mainVideoPath;
    QString _embededVideoPath;

    QScopedPointer<Video> _mainVideo;
    QScopedPointer<Video> _embededVideo;

    QScopedPointer<CycledBarrier> _barrier;
};

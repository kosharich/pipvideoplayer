#include "ExecutionHelpers.h"
#include "Application.h"

#include <QMessageBox>

namespace
{
    int appFailedExit()
    {
        qApp->exit(0);        
        return 0;
    }
}

void showErrorMessageBox(const QString& message)
{
    QMessageBox messageBox;
    messageBox.critical(0,"Error", message);
    messageBox.setFixedSize(500, 200);
    messageBox.exec();
}
    
int executeOrDie(Application* app)
{
    if (app == 0)
    {
        showErrorMessageBox("Application is null");
        appFailedExit();
    }
    
    try
    {
        return app->exec();
    }
    catch (const std::exception& ex)
    {   
        showErrorMessageBox(ex.what());   
        appFailedExit();
    }
    catch (...)
    {
        showErrorMessageBox("<unknown non-standard exception>");
        appFailedExit();
    }

    return 0;
}

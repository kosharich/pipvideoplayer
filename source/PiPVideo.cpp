#include "PiPVideo.h"
#include "Exception.h"
#include "Video.h"
#include "Frame.h"
#include "Barrier.h"

#include <QThread>
#include <QApplication>


namespace
{
    void ensureVideoPathIsValid(const QString& path)
    {
        if (path.isEmpty())
            throw Exception("A video path shouldn't be emty.");
    }

    void ensureFrameIsValid(const Frame& frame)
    {
        if (!frame.isValid())
            throw Exception("Frame is invalid: " + frame.error().toStdString());
    }

    void resizeIfNeed(Frame* frame, size_t maxWidth, size_t maxHeight)
    {
        if (frame->width() <= maxWidth && frame->height() <= maxHeight)
            return;

        const float scaleFactor = (float)maxWidth / (float)frame->width();

        frame->resize(maxWidth, frame->height() * scaleFactor);
    }
}

PiPVideo::PiPVideo(const QString mainVideoPath, const QString embededVideoPath)
    : _mainVideoPath(mainVideoPath)
    , _embededVideoPath(embededVideoPath)
{
    ensureVideoPathIsValid(mainVideoPath);
    ensureVideoPathIsValid(embededVideoPath);

    _barrier.reset(new CycledBarrier(2));

    _mainVideo.reset(new Video(_mainVideoPath, this));
    _embededVideo.reset(new Video(_embededVideoPath, this));

    _mainVideo->initialize(fps());
    _embededVideo->initialize(fps());
}

PiPVideo::~PiPVideo()
{
    _barrier->reset();
    
    _mainVideo.reset(); 
    _embededVideo.reset();
}

Frame* PiPVideo::getFrame()
{
    waitFrames();
    
    Frame* frame = _mainVideo->takeFrame();
    QScopedPointer<Frame> embededFrame(_embededVideo->takeFrame());

    ensureFrameIsValid(*frame);
    ensureFrameIsValid(*embededFrame);
    
    const uint embededFrameMaxWidth = frame->width() / 4;
    const uint embededFrameMaxHeight = frame->height() / 4;

    resizeIfNeed(embededFrame.data(), embededFrameMaxWidth, embededFrameMaxHeight);

    const uint xOffset = 30;
    const uint yOffset = 30;

    frame->insert(*embededFrame, xOffset, yOffset);

    const uint borderWidth = 4;
    QRect borderRect(
        xOffset - borderWidth / 2, yOffset - borderWidth / 2,
        embededFrame->width() + borderWidth, embededFrame->height() + borderWidth);
    frame->drowRectangle(borderRect, Qt::black, borderWidth);

    return frame;
}

double PiPVideo::fps() const
{
    return std::max(_mainVideo->fps(), _embededVideo->fps());
}

void PiPVideo::notify()
{
    _barrier->wait();
}

void PiPVideo::waitFrames()
{
    _barrier->wait();
}


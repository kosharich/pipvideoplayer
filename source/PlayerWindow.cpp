#include "PlayerWindow.h"
#include "Player.h"
#include "PlayerControl.h"
#include "ExecutionHelpers.h"

#include <QVBoxLayout>
#include <QApplication>


QString STYLESHEET =
    "#Video { background: black; min-width: 500px; min-height: 300px;}"
    "#StartButton { min-width: 100px; max-width: 100px; }"
    "#MainVideoButton { min-width: 200px; max-width: 200px; }"
    "#EmbededVideoButton { min-width: 200px; max-width: 200px; }"
    "#MainVideoLabel { margin-left: 6px; }"
    "#EmbededVideoLabel { margin-left: 6px; }"
    ;

PlayerWindow::PlayerWindow()
    : _player(new Player)
    , _playerControl(new PlayerControl)
{
    configureUi();

    connect(_playerControl, &PlayerControl::mainVideoSet, _player.data(), &Player::setMainVideo);
    connect(_playerControl, &PlayerControl::embededVideoSet, _player.data(), &Player::setEmbededVideo);

    connect(_playerControl, &PlayerControl::started, this, &PlayerWindow::start);
    connect(_playerControl, &PlayerControl::stoped, this, &PlayerWindow::stop);
}

PlayerWindow::~PlayerWindow()
{
}

void PlayerWindow::configureUi()
{
    QVBoxLayout* mainLayout = new QVBoxLayout;

    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    
    mainLayout->addWidget(_player->widget(), 1);
    mainLayout->addWidget(_playerControl);

    _player->widget()->setObjectName("Video");
    
    _playerControl->setObjectName("PlayerControl");

    setStyleSheet(STYLESHEET);

    setLayout(mainLayout);
}

void PlayerWindow::start()
{
    try
    {
        _player->start();
    }
    catch (const std::exception& ex)
    {   
        showErrorMessageBox(ex.what());   
        qApp->exit(1);
    }
    catch (...)
    {
        showErrorMessageBox("<unknown non-standard exception>");
        qApp->exit(1);
    }
}

void PlayerWindow::stop()
{
    _player->stop();
}

#include "Frame.h"

#include "opencv2/opencv.hpp"

#include <iostream>

Frame::Frame(const cv::Mat& mat)
    : _mat(mat)
{
}

Frame::Frame(const Frame& other)
    : _mat(other._mat.clone())
{
}

Frame::Frame(const QString& error)
    : _error(error)
{
}

Frame::~Frame()
{
}

const uchar* Frame::data() const
{
  return _mat.data;
}

size_t Frame::dataStep() const
{
    return _mat.step;
}

size_t Frame::width() const
{
    return _mat.cols;
}

size_t Frame::height() const
{
    return _mat.rows;
}

void Frame::resize(size_t width, size_t height)
{
    cv::Mat newMat;
    
    cv::resize(_mat, newMat, cv::Size(width, height));

    _mat = newMat;
}

void Frame::insert(const Frame& frame, uint xOrigin, uint yOrigin)
{
    const cv::Rect rect(xOrigin, yOrigin, frame.width(), frame.height());
    frame._mat.copyTo(_mat(rect));
}

namespace
{
    cv::Rect toCVRect(const QRect& rect)
    {
        return cv::Rect(rect.x(), rect.y(), rect.width(), rect.height());
    }

    cv::Scalar toCVColor(const QColor& color)
    {
        return cv::Scalar(color.green(), color.blue(), color.red());
    }
}

void Frame::drowRectangle(const QRect& rect, const QColor& color, uint width)
{
    cv::rectangle(_mat, toCVRect(rect), toCVColor(color), width);
}

#pragma once

#include <QLabel>


class ImageViewer : public QLabel
{
public:
    explicit ImageViewer(QWidget* parent = 0);
    virtual ~ImageViewer();

    void setImage(const QPixmap& image);
};

#include "Application.h"
#include "ExecutionHelpers.h"


int main(int argc, char* argv[])
{
  Application app(argc, argv);

  return executeOrDie(&app);
}

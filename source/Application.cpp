#include "Application.h"
#include "PlayerWindow.h"


Application::Application(int& argc, char** argv)
    : QApplication(argc, argv)
{}

int Application::exec()
{
    PlayerWindow player;
    player.show();

    return QApplication::exec();
}

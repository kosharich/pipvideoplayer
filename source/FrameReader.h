#pragma once

#include <QObject>
#include <QAtomicPointer>

class Frame;
class IFrameSequence;
class IFrameReadyNotifier;

class QThread;


class FrameReader : public QObject
{
    Q_OBJECT

public:
    FrameReader(IFrameSequence* frameSequence, IFrameReadyNotifier* frameReadyNotifier);
    virtual ~FrameReader();

    Frame* takeFrame();
    bool isFrameReady() const;

private slots:
    void threadProcedure();

private:
    QString errorMessage() const;
    void setErrorMessage(const QString& message);

private:
    IFrameSequence* _frameSequence;
    IFrameReadyNotifier* _frameNotifier;

    QAtomicPointer<Frame> _frame;

    QThread* _thread;
    QAtomicInt _doWork;
};

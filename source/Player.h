#pragma once

#include <QObject>
#include <QScopedPointer>
#include <QImage>

class ImageViewer;
class PiPVideo;
class Frame;

class QTimer;
class QPixmap;

class Player : public QObject
{
    Q_OBJECT

public:
    Player();
    virtual ~Player();

    QWidget* widget() const;

    void start();
    void stop();

public slots:
    void setMainVideo(const QString& mainVideo);
    void setEmbededVideo(const QString& embededVideo);

private slots:
    void onTimerTimeout();

private:
    void showNextFrame();
    void ensureNotStarted();

private:
    QString _mainVideo;
    QString _embededVideo;

    bool _isStarted;

    QScopedPointer<ImageViewer> _imageViewer;
    QScopedPointer<QPixmap> _image;

    QScopedPointer<PiPVideo> _video;
    QScopedPointer<Frame> _frame;

    QScopedPointer<QTimer> _frameTimer;
    uint _frameTimeout;
};

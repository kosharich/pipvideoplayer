#include "ImageHelpers.h"
#include "Exception.h"

#include <QPixmap>

#include <iostream>


namespace
{
    QImage frameToImage(const Frame& frame)
    {
        QImage img(frame.data(), frame.width(), frame.height(), frame.dataStep(), QImage::Format_RGB888);
        return img.rgbSwapped();
    }
}

void loadFromFrame(QPixmap* pixmap, const Frame& frame)
{
    const QImage& image = frameToImage(frame); 
    const bool succeded =  pixmap->convertFromImage(image);

    if (!succeded)
        throw Exception("Can not load QPixmap. Invalid Frame object.");
}

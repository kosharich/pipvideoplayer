#include "PlayerControl.h"

#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QFileInfo>


PlayerControl::PlayerControl(QWidget* parent)
    : QWidget(parent)
{
    configureUi();

    connect(_mainVideoButton, &QPushButton::clicked, this, &PlayerControl::chooseMainVideoFile);
    connect(_embededVideoButton, &QPushButton::clicked, this, &PlayerControl::chooseEmbededVideoFile);

    connect(_startButton, &QPushButton::clicked, this, &PlayerControl::onStartButtonClicked);
}

PlayerControl::~PlayerControl()
{}

namespace
{
    template<typename LayoutT>
    LayoutT* createLayout()
    {
        LayoutT* layout = new LayoutT;

        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);

        return layout;
    }
    
    QHBoxLayout* createOpenFileLayout(QLabel* filePath, QPushButton* openFileButton)
    {
        Q_ASSERT(filePath != 0);
        Q_ASSERT(openFileButton != 0);

        QHBoxLayout* layout = createLayout<QHBoxLayout>();

        layout->addWidget(openFileButton);
        layout->addWidget(filePath);

        return layout;
    }

    QLayout* createMainLayout(
        QHBoxLayout* openMainFileLayout, QHBoxLayout* openEmbededFileLayout, QPushButton* startButton)
    {
        Q_ASSERT(openMainFileLayout != 0);
        Q_ASSERT(openEmbededFileLayout != 0);
        Q_ASSERT(startButton != 0);

        QVBoxLayout* openFilesLayout = createLayout<QVBoxLayout>();

        openFilesLayout->addLayout(openMainFileLayout);
        openFilesLayout->addLayout(openEmbededFileLayout);

        QHBoxLayout* mainLayout = createLayout<QHBoxLayout>();
        
        mainLayout->addWidget(startButton);
        mainLayout->addLayout(openFilesLayout);

        return mainLayout;
    }
}

void PlayerControl::configureUi()
{
    _mainVideoLabel = new QLabel;
    _mainVideoLabel->setObjectName("MainVideoLabel");
    
    _embededVideoLabel = new QLabel;
    _embededVideoLabel->setObjectName("EmbededVideoLabel");

    _mainVideoButton = new QPushButton("Choose main video");
    _mainVideoButton->setObjectName("MainVideoButton");
    
    _embededVideoButton = new QPushButton("Choose embeded video");
    _embededVideoButton->setObjectName("EmbededVideoButton");

    _startButton = new QPushButton;
    _startButton->setCheckable(true);
    _startButton->setObjectName("StartButton");
    updateStartButton();

    QHBoxLayout* mainVideoLayout = createOpenFileLayout(_mainVideoLabel, _mainVideoButton);
    QHBoxLayout* embededVideoLayout = createOpenFileLayout(_embededVideoLabel, _embededVideoButton);

    setLayout(createMainLayout(mainVideoLayout, embededVideoLayout, _startButton));
}

void PlayerControl::updateStartButton()
{
    _startButton->setEnabled(!_mainVideoLabel->text().isEmpty() && !_embededVideoLabel->text().isEmpty());
    _startButton->setText(_startButton->isChecked() ? "Stop" : "Start");
}

void PlayerControl::updateVideoButtons()
{
    const bool isFileChoosingAvailable = !_startButton->isChecked();

    _mainVideoButton->setEnabled(isFileChoosingAvailable);
    _embededVideoButton->setEnabled(isFileChoosingAvailable);
}

namespace
{
    QString getFileName(const QString& path)
    {
        QFileInfo info(path);

        return info.baseName();
    }
}


void PlayerControl::chooseMainVideoFile()
{
    const QString filePath = QFileDialog::getOpenFileName();

    if (filePath.isEmpty())
        return;
    
    _mainVideoLabel->setText(getFileName(filePath));
    _mainVideoLabel->setToolTip(filePath);

    updateStartButton();

    emit mainVideoSet(filePath);
}

void PlayerControl::chooseEmbededVideoFile()
{
    const QString filePath = QFileDialog::getOpenFileName();

    if (filePath.isEmpty())
        return;
    
    _embededVideoLabel->setText(getFileName(filePath));
    _embededVideoLabel->setToolTip(filePath);

    updateStartButton();

    emit embededVideoSet(filePath);
}

void PlayerControl::onStartButtonClicked()
{
    updateStartButton();
    updateVideoButtons();

    if (_startButton->isChecked())
        emit started();
    else
        emit stoped();
}

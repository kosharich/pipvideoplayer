#pragma once

#include "opencv2/opencv.hpp"

#include <QScopedPointer>
#include <QString>
#include <QRect>
#include <QColor>

class Frame
{
public:
    Frame(const cv::Mat& mat);
    Frame(const Frame& other);
    Frame(const QString& error);

    ~Frame();

    const uchar* data() const;
    size_t dataStep() const;

    size_t width() const;
    size_t height() const;

    void resize(size_t width, size_t height);

    void insert(const Frame& frame, uint xOrigin, uint yOrigin);

    void drowRectangle(const QRect& rect, const QColor& color, uint width);

    bool isValid() const { return _error.isEmpty(); }
    const QString& error() const { return _error; }

private:
    cv::Mat _mat;

    QString _error;
};

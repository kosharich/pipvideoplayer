#pragma once

#include <exception>

class Exception : public std::exception
{
public:
    Exception(const std::string& message) throw()
        : _message(message)
    {}

    virtual ~Exception() throw() {}

    virtual const char* what() const throw() { return _message.data(); }

private:
    std::string _message;
};

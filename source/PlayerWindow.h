#pragma once

#include <QScopedPointer>
#include <QWidget>


class Player;
class PlayerControl;


class PlayerWindow : public QWidget
{
public:
    PlayerWindow();
    ~PlayerWindow();

private:
    void configureUi();

private slots:
    void start();
    void stop();

private:
    QScopedPointer<Player> _player;
    PlayerControl* _playerControl;
};

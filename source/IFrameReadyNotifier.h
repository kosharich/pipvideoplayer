#pragma once


class IFrameReadyNotifier
{
public:
    virtual void notify() = 0;
};

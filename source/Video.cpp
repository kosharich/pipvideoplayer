#include "Video.h"
#include "Frame.h"
#include "Exception.h"
#include "FrameReader.h"
#include "IFrameReadyNotifier.h"

#include "opencv2/opencv.hpp"

#include <QThread>
#include <QMutexLocker>


Video::Video(const QString& path, IFrameReadyNotifier* frameNotifier)
    : _isInitialized(false)
    , _desiredFps(0.0)
    , _desiredFrameCounter(0)
{
    _video.reset(new cv::VideoCapture(path.toStdString()));
    ensureVideoOpened();

    _frameReader.reset(new FrameReader(this, frameNotifier));
}

Video::~Video()
{
}

void Video::initialize(double desiredFps)
{
    ensureNotInitialized();

    _desiredFps = desiredFps;

    _isInitialized = true;
}

Frame* Video::takeFrame()
{
    ensureInitialized();

    return _frameReader->takeFrame();
}

double Video::fps() const
{
    QMutexLocker lock(&_mutex);

    return _video->get(CV_CAP_PROP_FPS);
}

// Run on another thread.
Frame* Video::getNextFrame()
{
    while (needUpdateFrame())
    {
        cv::Mat mat;

        bool succeded = false;

        {
            QMutexLocker lock(&_mutex);

            succeded = _video->read(mat);
        }

        if (!succeded)
            throw Exception("Frame can not be read.");

        _lastFrame.reset(new Frame(mat));
    }

    ++_desiredFrameCounter;

    return new Frame(*_lastFrame);
}

void Video::ensureVideoOpened() const
{
    if (!_video->isOpened())
        throw Exception("Can not open video");
}

void Video::ensureInitialized() const
{
    if (!_isInitialized)
        throw Exception("Video has not been initialized");
}
void Video::ensureNotInitialized() const
{
    if (_isInitialized)
        throw Exception("Video has been already initialized");
}

double Video::currentPositionMs() const
{
    QMutexLocker lock(&_mutex);

    return _video->get(CV_CAP_PROP_POS_MSEC);
}

bool Video::needUpdateFrame()
{
    return _lastFrame == 0 || currentPositionMs() < (1000.0 /_desiredFps) * (float)_desiredFrameCounter;
}

#include "Player.h"
#include "Video.h"
#include "ImageViewer.h"
#include "ImageHelpers.h"
#include "PiPVideo.h"
#include "ExecutionHelpers.h"
#include "Exception.h"

#include <QTimer>
#include <QApplication>

#include <iostream>


Player::Player()
    : _isStarted(false)
    , _imageViewer(new ImageViewer)
    , _image(new QPixmap)
    , _frameTimer(new QTimer)
    , _frameTimeout(0)
{
    connect(_frameTimer.data(), &QTimer::timeout, this, &Player::onTimerTimeout);
}

Player::~Player()
{
}

void Player::setMainVideo(const QString& mainVideo)
{
    ensureNotStarted();

    _mainVideo = mainVideo;
}

void Player::setEmbededVideo(const QString& embededVideo)
{
    ensureNotStarted();

    _embededVideo = embededVideo;
}

void Player::start()
{
    ensureNotStarted();

    _video.reset(new PiPVideo(_mainVideo, _embededVideo));

    _frameTimeout = (1000.0 / _video->fps());
    _frameTimer->start(_frameTimeout);

    _isStarted = true;
}

void Player::stop()
{
    _frameTimer->stop();
    _video.reset();

    _isStarted = false;
}

QWidget* Player::widget() const
{
    QWidget* widget = _imageViewer.data();

    return widget;
}

void Player::onTimerTimeout()
{
    try
    {
        showNextFrame();
    }
    catch (const std::exception& ex)
    {   
        showErrorMessageBox(ex.what());   
        qApp->exit(1);
    }
    catch (...)
    {
        showErrorMessageBox("<unknown non-standard exception>");
        qApp->exit(1);
    }
}

void Player::showNextFrame()
{
    Q_ASSERT(_video != 0);
    
    _frameTimer->start(_frameTimeout);

    QScopedPointer<Frame> newFrame;
    
    try
    {
        newFrame.reset(_video->getFrame());

        loadFromFrame(_image.data(), *newFrame);
    }
    catch (...)
    {
        throw;
    }

    _imageViewer->setPixmap(*_image);
    _imageViewer->setMinimumSize(newFrame->width(), newFrame->height());
    
    _frame.swap(newFrame);  
}

void Player::ensureNotStarted()
{
    if (_isStarted)
        throw Exception("Player has been already started");
}

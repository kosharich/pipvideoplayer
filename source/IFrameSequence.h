# pragma once

class Frame;

class IFrameSequence
{
public:
    virtual Frame* getNextFrame() = 0;
};

#include "ImageViewer.h"

#include <QPixmap>

#include <iostream>

ImageViewer::ImageViewer(QWidget* parent)
  : QLabel(parent)
{
    setAlignment(Qt::AlignCenter);
}

void ImageViewer::setImage(const QPixmap& image)
{
  setPixmap(image);
}

ImageViewer::~ImageViewer()
{}

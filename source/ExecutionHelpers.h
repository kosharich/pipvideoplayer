#pragma once

class Application;

#include <QString>

void showErrorMessageBox(const QString& message);

int executeOrDie(Application* app);

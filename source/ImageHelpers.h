#pragma once

#include "Frame.h"


class QPixmap;

void loadFromFrame(QPixmap* pixmap, const Frame& frame);

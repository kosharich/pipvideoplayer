#pragma once

#include <QMutex>
#include <QWaitCondition>


class CycledBarrier
{
public:
    CycledBarrier(uint count);
    ~CycledBarrier();

    void wait();
    void reset();

private:
    uint _count;
    uint _counter;
    bool _isReset;

    QMutex _mutex;
    QWaitCondition _waitCondition;
};

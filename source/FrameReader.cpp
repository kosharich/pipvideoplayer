#include "FrameReader.h"
#include "Exception.h"
#include "Frame.h"
#include "IFrameSequence.h"
#include "IFrameReadyNotifier.h"

#include <QThread>
#include <QMutexLocker>

#include <iostream>

FrameReader::FrameReader(IFrameSequence* frameSequence, IFrameReadyNotifier* frameNotifier)
    : _frameSequence(frameSequence)
    , _frameNotifier(frameNotifier)
    , _doWork(1)
{
    if (_frameSequence == 0)
        throw Exception("Argument 'frameSequence' can not be null");

    if (_frameNotifier == 0)
        throw Exception("Argument 'frameNotifier' can not be null");        

    _thread = new QThread;

    this->moveToThread(_thread);

    connect(_thread, &QThread::started, this, &FrameReader::threadProcedure);
    connect(_thread, &QThread::finished, _thread, &QThread::deleteLater);

    _thread->start();
}

FrameReader::~FrameReader()
{
    _doWork.store(0);

    _thread->quit();
    _thread->wait();

    _thread = 0;
}

bool FrameReader::isFrameReady() const
{
    Frame* frame = _frame.load();
    
    return frame != 0;
}

Frame* FrameReader::takeFrame()
{
    Frame* frame = _frame.fetchAndStoreRelaxed(0);

    if (frame == 0)
        throw Exception("Frame is not ready yet");

    return frame; 
}

void FrameReader::threadProcedure()
{
    while (_doWork == 1)
    {
        if (_frame.testAndSetOrdered(0, 0))
        {
            Frame* frame = 0;
            
            try
            {
                frame = _frameSequence->getNextFrame();
            }
            catch (const std::exception& ex)
            {
                frame = new Frame(ex.what());
            }
            catch (...)
            {
                frame = new Frame("<unknown non-standard exception>");
            }

            _frame.storeRelease(frame);
            _frameNotifier->notify();
        }

        QThread::msleep(1);
    }
}


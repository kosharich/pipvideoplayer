#pragma once

#include <QWidget>

class QLabel;
class QPushButton;

class PlayerControl : public QWidget
{
    Q_OBJECT

public:
    PlayerControl(QWidget* parent = 0);
    virtual ~PlayerControl();

signals:
    void mainVideoSet(const QString& mainVideoPath);
    void embededVideoSet(const QString& embededVideoPath);

    void started();
    void stoped();

private:
    void configureUi();
    void updateStartButton();
    void updateVideoButtons();

private slots:
    void chooseMainVideoFile();
    void chooseEmbededVideoFile();
    void onStartButtonClicked();

private:
    QLabel* _mainVideoLabel;
    QLabel* _embededVideoLabel;

    QPushButton* _mainVideoButton;
    QPushButton* _embededVideoButton;

    QPushButton* _startButton;
};

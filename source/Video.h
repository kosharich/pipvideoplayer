#pragma once

#include "IFrameSequence.h"

#include <QScopedPointer>
#include <QImage>
#include <QObject>
#include <QMutex>

class Frame;
class FrameReader;
class IFrameReadyNotifier;

namespace cv
{
    class VideoCapture;
}

class Video : public QObject, private IFrameSequence
{
public:
    Video(const QString& path, IFrameReadyNotifier* frameNotifier);

    virtual ~Video();

    void initialize(double desiredFps);

    Frame* takeFrame();

    double fps() const;

    double desiredFps() const { return _desiredFps; }

private:
    virtual Frame* getNextFrame();

private:
    void ensureVideoOpened() const;
    void ensureInitialized() const;
    void ensureNotInitialized() const;

    uint framesCount() const;
    double currentPositionMs() const;

    bool needUpdateFrame();

private:
    bool _isInitialized;

    QScopedPointer<cv::VideoCapture> _video;
    QScopedPointer<FrameReader> _frameReader;
    QScopedPointer<Frame> _lastFrame;

    mutable QMutex _mutex;

    double _desiredFps;
    uint _desiredFrameCounter;
};
